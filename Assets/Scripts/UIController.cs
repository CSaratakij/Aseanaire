﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Aseanaire
{
    public class UIController : MonoBehaviour
    {
        [SerializeField]
        GameController gameController;

        [SerializeField]
        Text txtTimer;

        [SerializeField]
        GameObject summaryUI;
        
        [SerializeField]
        Text txtQuestion;

        [SerializeField]
        Text[] txtChoices;

        [SerializeField]
        Text txtTotalQuestion;

        [SerializeField]
        Text txtAnswerCorrect;

        [SerializeField]
        Text txtAnswerIncorrect;

        [SerializeField]
        Button[] btnChoices;

        [SerializeField]
        Image[] correctIndicator;

        [SerializeField]
        Color correctBG;

        [SerializeField]
        Color correctFG;

        [SerializeField]
        Color inCorrectBG;

        [SerializeField]
        Color inCorrectFG;

        [SerializeField]
        Color defaultBG;

        [SerializeField]
        Color defaultFG;

        [SerializeField]
        Text txtCurrentScore;

        [SerializeField]
        Text txtSummaryScore;


        public UIController()
        {
            txtChoices = new Text[Global.MAX_CHOICE_PER_GAME];
        }

        void Update()
        {
            if (gameController.IsGameStart)
            {
                txtTimer.text = gameController.Timer.CurrentTimeToString();
                txtQuestion.text = gameController.CurrentQuestion.Description;
                txtTotalQuestion.text = "Question : " + (gameController.CurrentQuestionID + 1) + " Of " + Global.MAX_QUESTION_PER_GAME;

                for (int i = 0; i < txtChoices.Length; i++) {
                    var label = (i + 1) + ". " + gameController.CurrentQuestion.Choices[i];
                    txtChoices[i].text = label;
                }

                if (gameController.IsCheckedAnswer) {
                    var isCorrect = gameController.AnswerResults[gameController.CurrentQuestionID];

                    txtAnswerCorrect.gameObject.SetActive(isCorrect);
                    txtAnswerIncorrect.gameObject.SetActive(!isCorrect);

                    correctIndicator[gameController.CurrentQuestion.CorrectChoiceID].gameObject.SetActive(true);
                    txtCurrentScore.text = gameController.CurrentScore.ToString();

                    if (isCorrect) {
                        txtChoices[gameController.CurrentQuestionAnswerID].color = correctFG;
                        btnChoices[gameController.CurrentQuestionAnswerID].image.color = correctBG;
                    } else {
                        txtChoices[gameController.CurrentQuestionAnswerID].color = inCorrectFG;
                        btnChoices[gameController.CurrentQuestionAnswerID].image.color = inCorrectBG;
                    }


                    foreach (Button button in btnChoices) {
                        button.interactable = false;
                    }

                } else {
                    foreach (Button button in btnChoices) {
                        button.interactable = true;
                        button.colors = ColorBlock.defaultColorBlock;
                        button.image.color = defaultBG;
                    }

                    foreach (Image img in correctIndicator) {
                        img.gameObject.SetActive(false);
                    }

                    foreach (Text text in txtChoices) {
                        text.color = defaultFG;
                    }
                }

            } else {
                txtTimer.text = "00:00";
            }

            if (gameController.IsGameInit && gameController.IsGameStart && gameController.IsGameOver) {
                summaryUI.gameObject.SetActive(true);
                txtSummaryScore.text = gameController.CurrentScore.ToString();
                gameController.GameReset();
            }
        }
    }
}
