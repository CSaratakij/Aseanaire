﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aseanaire
{
    public class Timer : MonoBehaviour
    {
        [SerializeField]
        float currentTime;

        [SerializeField]
        float maxTime;


        public  int CurrentMinute { get { return (int)Mathf.Floor(currentTime / 60); } }
        public  float CurrentSecond { get { return ((currentTime / 60) - CurrentMinute) * 60; } }
        public  float CurrentTime { get { return currentTime; } }
        public  float MaxTime { get { return maxTime; } }
        public  bool IsTimeout { get { return currentTime <= 0; } }
        public  bool IsStop { get { return _isStop; } }


        bool _isStop;


        public Timer()
        {
            _isStop = true;
        }

        public string CurrentTimeToString()
        {
            var minute = "";
            var second = "";

            if (CurrentMinute < 10) {
                minute += "0";
            }

            if (CurrentSecond < 10) {
                second += "0";
            }

            minute += CurrentMinute;
            second += CurrentSecond;

            return minute + " : " + second;
        }

        public void SetMaxTime(float value)
        {
            maxTime = value;
        }

        public void Reset()
        {
            _isStop = true;
            currentTime = maxTime;
        }

        public void StartCountdown()
        {
            _isStop = false;
            StartCoroutine(_Countdown());
        }

        public void Stop()
        {
            _isStop = true;
            StopCoroutine(_Countdown());
            Debug.Log("Demand Stop...");
        }

        IEnumerator _Countdown()
        {
            while (currentTime > 0) {
                yield return new WaitForSeconds(1);
                if (!_isStop) {
                    currentTime -= 1;
                }
            } 
            Stop();
        }
    }
}
