﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Aseanaire
{
    public class Question
    {
        public string Country { get { return _country; } }
        public string Category { get { return _category; } }
        public string Description { get { return _description; } }
        public string[] Choices { get { return _choices; } }

        public int CorrectChoiceID { get { return _correctChoiceID; } }
        public string CorrectReason { get { return _correctReason; } }


        string _country;
        string _category;
        string _description;
        string[] _choices;

        int _correctChoiceID;
        string _correctReason;

        JObject _jsonObject;


        public Question()
        {
            _choices = new string[Global.MAX_CHOICE_PER_GAME];
        }

        public void LoadFromJson(string jsonString, string locale)
        {
            _jsonObject = JObject.Parse(jsonString);

            _country = (string)_jsonObject["country"];
            _category = (string)_jsonObject["category"];
            _correctChoiceID = (int)_jsonObject["correctChoiceID"];

            _description = (string)_jsonObject["locale"][locale]["question"];
            _correctReason = (string)_jsonObject["locale"][locale]["correctReason"];

            for (int i = 0; i < _choices.Length; i++) {
                _choices[i] = (string)_jsonObject["locale"][locale]["choices"][i];
            }
        }
    }
}
