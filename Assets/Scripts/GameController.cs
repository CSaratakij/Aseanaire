﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Aseanaire
{
    public class GameController : MonoBehaviour
    {
        [SerializeField]
        TextAsset questionFile;

        public Timer Timer { get { return _timer; } }

        public int CurrentQuestionID { get { return _currentQuestionID; } }
        public int CurrentQuestionAnswerID { get { return _currentQuestionAnswerID; } }
        public int CurrentScore { get { return _currentScore; } }

        public bool IsGameInit { get { return _isGameInit; } }
        public bool IsGameStart { get { return _isGameStart; } }
        public bool IsGameOver { get { return _isGameOver; } }
        public bool IsCheckedAnswer { get { return _isCheckedAnswer; } }
        public bool[] AnswerResults { get { return _answerResults; } }

        public Question CurrentQuestion { get { return _currentQuestion; } }


        int _currentQuestionID;
        int _currentQuestionAnswerID;

        int _currentScore;
        int _currentBonus;
        int _bonusCounter;

        bool _isGameInit;
        bool _isGameStart;
        bool _isGameOver;
        bool _isCheckedAnswer;
        bool _isFinishedAllQuestion;
        bool _isGamePaused;

        bool[] _answerResults;

        List<int> _placeBonus;
        List<int> _loadQuestionList;

        JArray _jsonQuestions;
        Question _currentQuestion;
        Timer _timer;


        public GameController()
        {
            _currentQuestionID = 0;
            _currentQuestionAnswerID = 0;
            _currentScore = 0;
            _currentBonus = 0;
            _bonusCounter = 0;
            _isGameInit = false;
            _isGameStart = false;
            _isGameOver = false;
            _isCheckedAnswer = false;
            _isFinishedAllQuestion = false;
            _isGamePaused = true;
            _currentQuestion = new Question();
            _answerResults = new bool[Global.MAX_QUESTION_PER_GAME];
            _placeBonus = new List<int>();
            _loadQuestionList = new List<int>();
        }

        public void GameReset()
        {
            _currentQuestionID = 0;
            _currentScore = 0;
            _currentBonus = 0;
            _bonusCounter = 0;
            _isGameInit = false;
            _isGameStart = false;
            _isGameOver = false;
            _isCheckedAnswer = false;
            _isFinishedAllQuestion = false;
            _isGamePaused = true;
            _currentQuestion = new Question();
            _timer.SetMaxTime(Global.MAX_TIME_PER_GAME_IN_MINUTE * 60);
            _timer.Reset();
            _placeBonus.Clear();
            _loadQuestionList.Clear();
        }

        public void GameStart()
        {
            _isGameInit = true;
            _isGameStart = true;
            _isGameOver = false;
            _isCheckedAnswer = false;
            _isFinishedAllQuestion = false;
            _isGamePaused = false;
            _timer.StartCountdown();
            GenerateLoadQuestionList();
            LoadQuestion(_loadQuestionList[_currentQuestionID]);
        }

        public void GamePause()
        {
            _isGamePaused = true;
            _timer.Stop();
        }

        public void GameResume()
        {
            _isGamePaused = false;
            _timer.StartCountdown();
        }

        public void GameOver()
        {
            _isGameOver = true;
        }

        public void SetCurrentQuestionAnswerID(int value)
        {
            _currentQuestionAnswerID = value;
        }

        public void CheckAnswer()
        {
            _answerResults[_currentQuestionID] = _currentQuestionAnswerID.Equals(_currentQuestion.CorrectChoiceID);
            _isCheckedAnswer = true;

            if (_answerResults[_currentQuestionID]) {
                _bonusCounter += 1;
                _currentScore += 100;
            } else {
                _bonusCounter = 0;
            }

            var isDeserveBonus = _bonusCounter.Equals(3);

            if (isDeserveBonus) {
                _placeBonus.Add(_currentQuestionID);
            }

            if ((_currentQuestionID + 1).Equals(Global.MAX_QUESTION_PER_GAME)) {
                _isFinishedAllQuestion = true;
            }
        }

        public void NextQuestion()
        {
            if ((_currentQuestionID + 1) < Global.MAX_QUESTION_PER_GAME) {
                _currentQuestionID += 1;
                _isCheckedAnswer = false;
                LoadQuestion(_currentQuestionID);
            } else {
                GameOver();
            }
        }

        void Awake()
        {
            _timer = GetComponent<Timer>();

            if (questionFile) {
                _jsonQuestions = JArray.Parse(questionFile.text);
            }
        }

        void Update()
        {
            if (_isGameInit && _isGameStart) {
                if (_timer.IsTimeout) {
                    _isGameOver = true;
                } else {
                    if (_isFinishedAllQuestion) {
                        _timer.Stop();
                    }
                }
            }
        }

        void LoadQuestion(int id)
        {
            _currentQuestion.LoadFromJson(_jsonQuestions[id].ToString(), GameSetting.currentLocale);
        }

        void GenerateLoadQuestionList()
        {
            JArray items = (JArray)_jsonQuestions;
            var refList = new List<int>();

            for (int i = 0; i < items.Count; i++) {
                refList.Add(i);
            }

            refList.Reverse();

            for (int i = 0; i < Global.MAX_QUESTION_PER_GAME; i++) {
                var resultIndex = (int)Random.Range(0, refList.Count - 1);
                _loadQuestionList.Add(refList[resultIndex]);
                refList.Remove(resultIndex);
            }
        }
    }
}
