﻿namespace Aseanaire
{
    public struct Global
    {
        public const int MAX_CATEGORY = 10;
        public const int MAX_COUNTRY = 10;
        public const int MAX_QUESTION_PER_GAME = 12;
        public const int MAX_CHOICE_PER_GAME = 4;
        public const float MAX_TIME_PER_GAME_IN_MINUTE = 10;

        public struct Country
        {
            public const string BRUNEI = "Brunei";
            public const string CAMBODIA = "Cambodia";
            public const string INDONESIA = "Indonesia";
            public const string LAOS = "Laos";
            public const string MALAYSIA = "Malaysia";
            public const string MYANMAR = "Myanmar";
            public const string PHILIPPINES = "Philippines";
            public const string SINGAPORE = "Singapore";
            public const string THAILAND = "Thailand";
            public const string VIETNAM = "Vietnam";
        }

        public struct Locale
        {
            public const string ENGLISH = "eng";
            public const string THAI = "th";
        }

        public struct Category
        {
            public const string CULTURE = "Culture";
            public const string ECONOMIC = "Economic";
            public const string EDUCATION = "Education";
            public const string ENTERTAINMENT = "Entertainment";
            public const string FOOD = "Food";
            public const string GEOGRAPHY = "Geography";
            public const string HISTORY = "History";
            public const string POLITIC = "Politic";
            public const string SOCIAL = "Social";
            public const string SPORT = "Sport";
        }

        public struct Rank
        {
            public const string BEGINNER = "Beginner";
            public const string AMATEUR = "Amateur";
            public const string EXPERT = "Expert";
            public const string MASTER = "Master";
        }
    }
}
